package com.cxc.commons.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter 
@NoArgsConstructor 
public class BillCreateCommand {

	private Long id;
	private Long currencyId;
	private String description;
	private int qty;
	private Long customerId;
}
