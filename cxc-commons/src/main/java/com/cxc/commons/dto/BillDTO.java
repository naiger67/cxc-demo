package com.cxc.commons.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter 
@NoArgsConstructor 
public class BillDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private CurrencyDTO currency;
	private String description;
	private int qty;
	private Long customerId;
}
