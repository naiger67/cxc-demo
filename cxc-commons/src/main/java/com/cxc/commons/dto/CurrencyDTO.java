package com.cxc.commons.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter 
@NoArgsConstructor 
public class CurrencyDTO {
	private Long id;
	private String name;
	
	public static CurrencyDTO make(String name) {
		CurrencyDTO currencyDTO = new CurrencyDTO();
		currencyDTO.setName(name);
		return currencyDTO;
	}
}
