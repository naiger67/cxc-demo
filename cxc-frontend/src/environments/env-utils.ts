export const ON = false;
export const OFF = true;

export function ApiRouter(_domain, _subdominio, debug) {
	return {
		domain() {
			return _domain;
		},
		url(protocol, subdominio, _endpoint) {
			let domain = this.domain() + "/";
			let endpoint = !subdominio ? _endpoint : subdominio + "/" + _endpoint;
			return protocol + domain + endpoint;
		},
		debug() {
			return debug;
		},
		resource(url, debug) {
			return { url: url, debug: debug };
		},
		route(_endpoint, debug?) {
			let _debug = arguments.length>1?debug:this.debug();
			return this.resource(
				this.url("http://", _subdominio, _endpoint),
				_debug
			);
		},
		routeHttps(_endpoint, debug?) {
      let _debug = arguments.length>1?debug:this.debug();
			return this.resource(
				this.url("https://", _subdominio, _endpoint),
				_debug
			);
		}
	}
}