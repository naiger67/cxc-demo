import { ApiRouter, ON, OFF } from "./env-utils";
const customer = ApiRouter("localhost:9000", "api/customer", ON);
const bills = ApiRouter("localhost:9001", "api/bills", ON);

export const environment = {
  production: false,
  allowedDomains: [],
  disallowedRoutes: [],
  customer: {
    find: customer.route(":id"), 
    all: customer.route("", OFF), 
    create: customer.route(""),
    update: customer.route(":id"),
    delete: customer.route(":id")
  },
  bills: {
    find: bills.route(":id", OFF), 
    all: bills.route("", OFF), 
    create: bills.route(""),
    update: bills.route(":id"),
    delete: bills.route(":id")
  }
};