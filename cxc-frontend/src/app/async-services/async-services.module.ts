import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerFind } from './customer/customer-find.services';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    CustomerFind
  ]
})
export class AsyncServicesModule { }
