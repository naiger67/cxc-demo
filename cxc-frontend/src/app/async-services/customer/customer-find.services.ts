import { Injectable } from "@angular/core";
import { ApiGateway } from "src/app/api-gateway/http/api-gateway";
import { environment } from "src/environments/environment";
import { ApiGatewayCommand } from "../../api-gateway/http/api-gateway-command";

@Injectable()

export class CustomerFind {

	constructor(private apiGateway: ApiGateway) { }

	handle(params: any) {
		let command = new ApiGatewayCommand({
			url: environment.customer.find.url.replace(":id", params.id),
			method: ApiGatewayCommand.METHOD_GET,
			params: {},
			headers: {},
			debug: environment.customer.find.debug,
			response: this.apiGateway.responseSuccessFake([])
		});

		return this.apiGateway.execute(command);
	}
}