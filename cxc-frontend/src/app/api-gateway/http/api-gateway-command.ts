import { ApiGatewayResponse } from './api-gateway-response';
import { throwError, of } from 'rxjs';

export interface ApiGatewayCommandSpec {
	method: string,
	url?: string,
	params?: any,
	headers?: any,
	debug?: boolean,
	response?: ApiGatewayResponse
}

export class ApiGatewayCommand {

	public static METHOD_GET = 'get';
	public static METHOD_POST = 'post';
	public static METHOD_PUT = 'put';
	public static METHOD_DELETE = 'delete';
	public static METHOD_HEAD = 'head';
	public static DEBUG_ON = true;
	public static DEBUG_OFF = false;

	private _method: string;
	private _url: string;
	private _headers;
	private _params;
	private _debug: boolean;
	private _response: ApiGatewayResponse;

	constructor(spec: ApiGatewayCommandSpec) {
		this.setMethod(spec.method);
		this.setUrl(spec.url);
		this.setHeaders(spec.headers || {});
		this.setParams(spec.params || {});
		this.setDebug(spec.debug);
		this.setResponse(spec.response);
	}

	method() {
		return this._method;
	}

	url() {
		return this._url;
	}

	headers() {
		return this._headers;
	}

	params() {
		return this._params;
	}

	debug() {
		return this._debug;
	}

	response() {
		return this._response;
	}

	private setMethod(method) {
		this.assertMethodIsCorrect(method);
		this._method = method;
	}

	private setUrl(url) {
		this._url = url;
	}

	private setHeaders(headers) {
		this._headers = headers;
	}

	private setParams(params) {
		this._params = params;
	}

	private setDebug(debug) {
		this._debug = (debug === true)
			? ApiGatewayCommand.DEBUG_ON
			: ApiGatewayCommand.DEBUG_OFF;
	}

	private setResponse(response) {
		this._response = response;
	}

	private assertMethodIsCorrect(method) {
		let methods = [
			ApiGatewayCommand.METHOD_GET,
			ApiGatewayCommand.METHOD_POST,
			ApiGatewayCommand.METHOD_PUT,
			ApiGatewayCommand.METHOD_DELETE,
			ApiGatewayCommand.METHOD_HEAD
		];

		let existsMethod = methods.includes(method);
		if (!existsMethod) throw new Error('Bad method params');
		return existsMethod;
	}

	responseFake() {
		let response = this.response();
		return response.hasError() ? throwError(response) : of(response);
	}

	configGet() {
		return {
			headers: this._headers,
			params: this._params,
			observe: 'response'
		};
	}

	configPost() {
		return {
			headers: this._headers,
			observe: 'response'
		}
	}

  toLog() {
    return {
			url: this._url,
      method: this._method,
      headers: this._headers,
      params: this._params
		};
  }
}