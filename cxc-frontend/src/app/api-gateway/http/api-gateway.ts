import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ApiGatewayCommand } from './api-gateway-command';
import { ApiGatewayResponse } from './api-gateway-response';
import { map, catchError } from 'rxjs/operators'
import { throwError, Observable } from 'rxjs';

@Injectable()
export class ApiGateway {

	constructor(private http: HttpClient) { }

	public execute(command: ApiGatewayCommand): Observable<ApiGatewayResponse> {
		return command.debug()
			? command.responseFake()
			: this.asyncAction(command);
	}

	private asyncAction(command) {
		let action = this[command.method()].bind(this);
		let observable = action(command);
		return observable.pipe(
			map((response: HttpResponse<any>) => {
				console.log(response);

				let apiGatewayResponse = ApiGatewayResponse.make(response);
				this.assertThatResponseNotHasError(apiGatewayResponse);
				return apiGatewayResponse;
			}),
			catchError((errorResponse: HttpErrorResponse) => {
				let apiResponseError = ApiGatewayResponse.makeErr(errorResponse);
				return throwError(apiResponseError);
			})
		);
	}

	private assertThatResponseNotHasError(apiGatewayResponse: ApiGatewayResponse) {
		if (!apiGatewayResponse.hasError()) {

			return;
		};
		throw apiGatewayResponse.toError();
	}

	private get(command) {
		return this.http.get(command.url(), command.configGet());
	}

	private post(command) {
		return this.http.post(command.url(), command.params(), command.configPost());
	}

	private put(command) {
		return this.http.put(command.url(), command.params(), command.configPost());
	}

	private delete(command) {
		return this.http.delete(command.url());
	}

	responseSuccessFake(dummyData) {
		let message = 'Success Fake Response With Dummy Data';
		return this.responseFake("00", message, dummyData, 200);
	}

	responseErrorLogicFake(dummyData) {
		let message = 'Error LOGIC Fake Response With Dummy Data';
		return this.responseFake("01", message, dummyData, 200);
	}

	responseErrorServerFake(dummyData) {
		let message = 'Error SERVER Fake Response With Dummy Data';
		return this.responseFake("012", message, dummyData, 500);
	}

	responseFake(code, msg, data, status) {
		return new ApiGatewayResponse(code, msg, data, status);
	}
}

