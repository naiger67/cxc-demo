import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerCreateRoutingModule } from './customer-create-routing.module';
import { CustomerCreateComponent } from './customer-create.component';
import { AsyncServicesModule } from 'src/app/async-services/async-services.module';


@NgModule({
  declarations: [CustomerCreateComponent],
  imports: [
    CommonModule,
    CustomerCreateRoutingModule,
    AsyncServicesModule
  ]
})
export class CustomerCreateModule { }
