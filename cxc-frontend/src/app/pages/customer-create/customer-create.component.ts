import { Component, OnInit } from '@angular/core';
import { CustomerFind } from 'src/app/async-services/customer/customer-find.services';

@Component({
  selector: 'app-customer-create',
  templateUrl: './customer-create.component.html',
  styleUrls: ['./customer-create.component.scss']
})
export class CustomerCreateComponent implements OnInit {

  constructor(private customerFind: CustomerFind) { }

  ngOnInit(): void {
    this.customerFindAction({id: 1000});
  }

  protected customerFindAction(params) {
    this.customerFind.handle(params).subscribe({
      next: this.customerFindActionOk.bind(this),
      error: this.customerFindActionErr.bind(this)
    });
  }

  protected customerFindActionOk(response) {
    let data = response.data();
    console.log("customerFindActionOk", data);
  }

  protected customerFindActionErr(err) {
    let data = err.data();
    console.log("customerFindActionErr", data);
  }
}
