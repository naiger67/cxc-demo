package com.cxc.bill.mappers;

import org.mapstruct.Mapper;

import com.cxc.bill.entities.Currency;
import com.cxc.commons.dto.CurrencyDTO;

@Mapper(componentModel = "spring")
public interface CurrencyMapper {
	Currency toEntity(CurrencyDTO source);
	CurrencyDTO toDto(Currency destination);
}
