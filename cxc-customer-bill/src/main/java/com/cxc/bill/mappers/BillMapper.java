package com.cxc.bill.mappers;

import org.mapstruct.Mapper;

import com.cxc.bill.entities.Bill;
import com.cxc.commons.dto.BillDTO;

@Mapper(componentModel = "spring")
public interface BillMapper {

	Bill toEntity(BillDTO source);
	
	BillDTO toDto(Bill destination);
}
