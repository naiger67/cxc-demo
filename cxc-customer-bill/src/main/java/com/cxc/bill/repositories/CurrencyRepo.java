package com.cxc.bill.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cxc.bill.entities.Currency;

public interface CurrencyRepo extends JpaRepository<Currency, Long> {

}