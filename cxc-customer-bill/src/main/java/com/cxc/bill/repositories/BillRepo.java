package com.cxc.bill.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.cxc.bill.entities.Bill;

public interface BillRepo extends JpaRepository<Bill, Long> {

	@Query(value = "SELECT * FROM bills b WHERE b.customer_id = ?1", nativeQuery = true)
	public List<Bill> whereByCustomerId(Long customerId);
}