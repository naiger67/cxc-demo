package com.cxc.bill.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cxc.bill.entities.Bill;
import com.cxc.bill.services.billCrud.BillCrud;
import com.cxc.commons.commands.BillCreateCommand;
import com.cxc.commons.dto.BillDTO;

@RestController
public class BillController {
	
	@Autowired
	protected BillCrud billCrud;
	
	@PostMapping("/api/bills")
	public BillDTO create(@RequestBody BillCreateCommand billCreateCommand) throws Exception {
		Bill bill = this.billCrud.create(billCreateCommand);
		return this.billCrud.mapout(bill);
	}
	
	@GetMapping("/api/bills/{id}")
	public BillDTO find(@PathVariable("id") Long id) throws Exception {
		Bill bill = this.billCrud.findById(id);
		return this.billCrud.mapout(bill);
	}
	
	@GetMapping("/api/bills")
	public List<BillDTO> findAll() {
		List<Bill> bills = this.billCrud.findAll();
		return this.billCrud.mapoutList(bills);
	}
	
	@GetMapping("/api/customer/{customerId}/bills")
	public List<BillDTO> findAllByCustomer(@PathVariable("customerId") Long customerId) {
		List<Bill> bills = this.billCrud.findAllByCustomer(customerId);
		return this.billCrud.mapoutList(bills);
	}
	
	@PutMapping("api/bills/{id}")
	public BillDTO update(@PathVariable("id") Long id, @RequestBody BillCreateCommand billCreateCommand) throws Exception {
		billCreateCommand.setId(id);
		Bill bill = this.billCrud.update(billCreateCommand);
		return this.billCrud.mapout(bill);
	}
	
	@DeleteMapping("/api/bills/{id}")
	public boolean delete(@PathVariable("id") Long id) throws Exception {
		this.billCrud.delete(id);
		return true;
	}
}
