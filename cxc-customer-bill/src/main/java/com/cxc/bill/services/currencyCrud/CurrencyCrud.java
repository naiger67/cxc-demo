package com.cxc.bill.services.currencyCrud;


import java.util.Optional;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cxc.bill.entities.Currency;
import com.cxc.bill.mappers.CurrencyMapper;
import com.cxc.bill.repositories.CurrencyRepo;
import com.cxc.commons.dto.CurrencyDTO;

@Service
public class CurrencyCrud {

	@Autowired
	protected CurrencyRepo currencyRepo;
	
	@Autowired
	protected CurrencyMapper currencyMapper;
	
	
	@Transactional
	public Currency create(CurrencyDTO command) {
		Currency currency = this.currencyMapper.toEntity(command);
		return this.currencyRepo.save(currency);
	}
	
	@Transactional(value = TxType.SUPPORTS)
	public boolean isSeed() {
		return this.currencyRepo.count() > 0;
	}
	
	@Transactional(value = TxType.SUPPORTS)
	public Currency findById(Long id) throws Exception {
		Optional<Currency> currencyX = this.currencyRepo.findById(id);
		if(!currencyX.isPresent()) throw new Exception("CurrencyNotFound");
		return currencyX.get();
	}
}
