package com.cxc.bill.services.customerCrud;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.cxc.commons.dto.CustomerDTO;

@FeignClient(name = "customer-service", url = "localhost:9000")
public interface ICustomerCrud {
	
	@GetMapping("/api/customer/{id}")
	public CustomerDTO find(@PathVariable Long id) throws Exception;
	
	@GetMapping("/api/customer")
	public List<CustomerDTO> findAll();
	
	@PostMapping("/api/customer")
	public CustomerDTO create(@RequestBody CustomerDTO command);
	
	@PutMapping("/api/customer/{id}") 
	public CustomerDTO update(@PathVariable Long id, @RequestBody CustomerDTO command) throws Exception;
	
	@DeleteMapping("/api/customer/{id}")
	public boolean delete(@PathVariable Long id) throws Exception;
}
