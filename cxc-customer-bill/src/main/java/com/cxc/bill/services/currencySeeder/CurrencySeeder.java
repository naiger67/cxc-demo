package com.cxc.bill.services.currencySeeder;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cxc.bill.services.currencyCrud.CurrencyCrud;
import com.cxc.commons.dto.CurrencyDTO;

@Component
public class CurrencySeeder {

	@Autowired
	CurrencyCrud currencyCrud;
	
    @PostConstruct
    public void init() {
    	if(currencyCrud.isSeed()) return;
    	this.currencyCrud.create(CurrencyDTO.make("MXN"));
    	this.currencyCrud.create(CurrencyDTO.make("USD"));
    	this.currencyCrud.create(CurrencyDTO.make("BSF"));    	    	
    }
}