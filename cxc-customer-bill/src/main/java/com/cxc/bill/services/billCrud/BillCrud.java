package com.cxc.bill.services.billCrud;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cxc.bill.entities.Bill;
import com.cxc.bill.entities.Currency;
import com.cxc.bill.mappers.BillMapper;
import com.cxc.bill.repositories.BillRepo;
import com.cxc.bill.services.currencyCrud.CurrencyCrud;
import com.cxc.bill.services.customerCrud.ICustomerCrud;
import com.cxc.commons.commands.BillCreateCommand;
import com.cxc.commons.dto.BillDTO;
import com.cxc.commons.dto.CustomerDTO;

@Service
public class BillCrud {

	@Autowired
	protected BillRepo billRepo;
	
	@Autowired
	protected CurrencyCrud currencyCrud;
	
	@Autowired
	protected ICustomerCrud customerCrud;
	
	@Autowired
	protected BillMapper billMapper;
	
	public Bill create(BillCreateCommand billCreateCommand) throws Exception {
		Currency currency = this.currencyCrud.findById(billCreateCommand.getCurrencyId());
		CustomerDTO customerDTO = this.customerCrud.find(billCreateCommand.getCustomerId());
		
		Bill bill = new Bill();
		bill.setCurrency(currency);
		bill.setCustomerId(customerDTO.getId());
		bill.setDescription(billCreateCommand.getDescription());
		bill.setQty(billCreateCommand.getQty());
		
		this.save(bill);
		
		return bill;
	}
	
	@Transactional
	public Bill findById(Long id) throws Exception {
		Optional<Bill> billX = this.billRepo.findById(id);
		if(!billX.isPresent()) throw new Exception("BillNotFound");
		return billX.get();
	}
	
	@Transactional(value = TxType.SUPPORTS)
	public List<Bill> findAllByCustomer(Long customerId) {
		return this.billRepo.whereByCustomerId(customerId);
	}
	
	public Bill update(BillCreateCommand billCreateCommand) throws Exception {
		Bill bill = this.findById(billCreateCommand.getId());
		Currency currency = this.currencyCrud.findById(billCreateCommand.getCurrencyId());
		CustomerDTO customerDTO = this.customerCrud.find(billCreateCommand.getCustomerId());

		bill.setCurrency(currency);
		bill.setCustomerId(customerDTO.getId());
		bill.setDescription(billCreateCommand.getDescription());
		bill.setQty(billCreateCommand.getQty());
		
		this.save(bill);
		
		return bill;
	}
	
	@Transactional(value = TxType.SUPPORTS)
	public List<Bill> findAll() {
		return this.billRepo.findAll();
	}
	
	@Transactional
	protected Bill save(Bill bill) {
		return this.billRepo.save(bill);
	}
	
	@Transactional
	public boolean delete(Long id) throws Exception {
		Bill billX = this.findById(id);
		this.billRepo.delete(billX);
		return true;
	}
	
	public BillDTO mapout(Bill bill) {
		return this.billMapper.toDto(bill);
	}
	
	public List<BillDTO> mapoutList(List<Bill> bills) {
		return bills.stream().map(this::mapout).collect(Collectors.toList());
	}
}
