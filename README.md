# Prueba Fullstack CXC

Proyectos asociados a la prueba.

## Instalación

Backend

```bash
> mysql -u root
mysql> CREATE DATABASE cxc_customer;
mysql> CREATE DATABASE cxc_customer_bill; 
```
Instalamos las bases de datos

```bash
> cd cxc-commons
> mvn clean install
```
Instalamos la librería commons en el repositorio local de maven

```bash
> cd cxc-eureka
> mvn spring:boot:run
```
Este servicio levanta en el puerto 8671

```bash
> cd cxc-customer
> mvn spring:boot:run
```
Este servicio levanta en el puerto 9000

```bash
> cd cxc-customer-bill
> mvn spring:boot:run
```
Este servicio levanta en el puerto 9001

## Instalación

Frontend
```bash
> cd cxc-frontend
> npm install
```

## License
[MIT]
