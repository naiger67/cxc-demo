package com.cxc.customer.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cxc.customer.entities.Customer;

public interface CustomerRepo extends JpaRepository<Customer, Long> {

}
