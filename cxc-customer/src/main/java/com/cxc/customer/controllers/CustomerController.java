package com.cxc.customer.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cxc.commons.dto.CustomerDTO;
import com.cxc.customer.entities.Customer;
import com.cxc.customer.mappers.CustomerMapper;
import com.cxc.customer.services.customerCrud.CustomerCrud;

@RestController
public class CustomerController {

	@Autowired
	protected CustomerCrud customerCrud;
	
	@Autowired
	protected CustomerMapper customerMapper;
	
	@GetMapping("/api/customer/{id}")
	public CustomerDTO find(@PathVariable Long id) throws Exception {
		Customer customer = this.customerCrud.findById(id);
		return this.customerMapper.toDto(customer);
	}
	
	@GetMapping("/api/customer")
	public List<CustomerDTO> findAll() {
		return this.customerCrud.findAllMapDto();
	}
	
	@PostMapping("/api/customer")
	public CustomerDTO create(@RequestBody CustomerDTO command) {
		Customer customer = this.customerCrud.create(command);
		return this.customerMapper.toDto(customer);
	}
	
	@PutMapping("/api/customer/{id}") 
	public CustomerDTO update(@PathVariable Long id, @RequestBody CustomerDTO command) throws Exception {
		Customer customer = this.customerCrud.update(id, command);
		return this.customerMapper.toDto(customer);
	}
	
	@DeleteMapping("/api/customer/{id}")
	public boolean delete(@PathVariable Long id) throws Exception {
		return this.customerCrud.delete(id);
	}
}
