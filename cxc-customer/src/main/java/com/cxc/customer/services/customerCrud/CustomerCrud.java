package com.cxc.customer.services.customerCrud;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cxc.commons.dto.CustomerDTO;
import com.cxc.customer.entities.Customer;
import com.cxc.customer.mappers.CustomerMapper;
import com.cxc.customer.repositories.CustomerRepo;


@Service
public class CustomerCrud {

	@Autowired
	protected CustomerRepo customerRepo;
	
	@Autowired
	protected CustomerMapper customerMapper;
	
	
	
	@Transactional
	public Customer findById(Long id) throws Exception {
		Optional<Customer> customerX = this.customerRepo.findById(id);
		if(!customerX.isPresent()) throw new Exception("CustomerNotFound");
		return customerX.get(); 
	}
	
	@Transactional(value = TxType.SUPPORTS)
	public List<Customer> findAll() {
		return this.customerRepo.findAll();
	}
	
	public List<CustomerDTO> findAllMapDto() {
		return this.findAll()
			.stream()
			.map(this.customerMapper::toDto)
			.collect(Collectors.toList());
	}
	
	@Transactional
	public Customer create(CustomerDTO customerDto) {
		Customer customer = this.customerMapper.toEntity(customerDto);
		this.customerRepo.save(customer);
		return customer;
	}
	
	@Transactional
	public Customer update(Long id, CustomerDTO customerDto) throws Exception {
		Customer customer = this.findById(id);
		customer.setName(customerDto.getName());
		customer.setAddress(customerDto.getAddress());
		customer.setPhone(customerDto.getPhone());
		customer.setRfc(customerDto.getRfc());
		
		this.customerRepo.save(customer);
		return customer;
	}
	
	public boolean delete(Long id) throws Exception {
		Customer customer = this.findById(id);
		this.customerRepo.delete(customer);
		return true;
	}
}
