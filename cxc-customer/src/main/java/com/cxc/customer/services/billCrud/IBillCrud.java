package com.cxc.customer.services.billCrud;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.cxc.commons.dto.BillDTO;

@FeignClient(name = "customer-bill-service", url = "localhost:9001")
public interface IBillCrud {
	
	@GetMapping("/api/customer/{customerId}/bills")
	public List<BillDTO> findAllByCustomer(@PathVariable("customerId") Long customerId);
}
