package com.cxc.customer.mappers;

import org.mapstruct.Mapper;

import com.cxc.commons.dto.CustomerDTO;
import com.cxc.customer.entities.Customer;

@Mapper(componentModel = "spring")
public interface CustomerMapper {
	Customer toEntity(CustomerDTO source);
	CustomerDTO toDto(Customer destination);	
}
