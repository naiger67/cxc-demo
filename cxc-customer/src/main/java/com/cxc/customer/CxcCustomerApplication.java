package com.cxc.customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@EnableEurekaClient
@EnableFeignClients
@ComponentScan("com.cxc")
@SpringBootApplication
public class CxcCustomerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CxcCustomerApplication.class, args);
	}

}
