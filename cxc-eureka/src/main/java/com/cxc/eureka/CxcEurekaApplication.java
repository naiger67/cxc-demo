package com.cxc.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class CxcEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CxcEurekaApplication.class, args);
	}

}
